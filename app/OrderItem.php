<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['product_id', 'quantity', 'discount', 'price'];


    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function sum()
    {
        return round(($this->price - ($this->price * ($this->discount/100))) * $this->quantity);
    }


}
