<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    public $timestamps = false;

    public function orders()
    {
        return $this->hasMany(Order::class, 'buyer_id', 'id');
    }
}
