<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderItems' => OrderItemResource::collection($this->orderItems),
            'buyer' => [
                'buyerFullName' => $this->buyer->name . ' ' . $this->buyer->surname,
                'buyerAddress' => $this->buyer->country . ' ' . $this->buyer->city . ' ' . $this->buyer->addressLine,
                'buyerPhone' => $this->buyer->phone
            ]
        ];
    }
}
