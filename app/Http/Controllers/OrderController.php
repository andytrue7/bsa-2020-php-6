<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class OrderController extends Controller
{

    public function index()
    {

    }


    public function store(Request $request)
    {
        $buyerId = $request->input('buyerId');
        $items = $request->input('orderItems');

        $order = Order::create(['buyer_id' => $buyerId]);

        if (!empty($items)) {
            foreach ($items as  $item) {
                $order->orderItems()->create([
                    'product_id'=> $item['productId'],
                    'quantity'=>$item['productQty'],
                    'discount' => $item['productDiscount'],
                    'price' => Product::find($item['productId'])->price
                ]);
            }
        }

        return \response($order);
    }


    public function show($id)
    {
        return new OrderResource(Order::find($id));
    }


    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        if (!$order) {
            new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }

        $orderId = $request->input('orderId');
        $items = $request->input('orderItems');

        $order->id = $orderId;

        if (empty($items)) {
            foreach ($order->orderItems() as $orderItem) {
                $orderItem->delete();
            }
        } else {
            foreach ($items as $item) {
                $order->orderItems()->update([
                    'product_id'=> $item['productId'],
                    'quantity'=>$item['productQty'],
                    'discount' => $item['productDiscount'],
                    'price' => Product::find($item['productId'])->price
                ]);
            }
        }

        $order->save();
    }


    public function destroy($id)
    {
        //
    }
}
