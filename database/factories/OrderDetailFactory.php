<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'product_id' => \App\Product::all()->random()->id,
        'quantity' => $faker->numberBetween(1, 10),
        'price' => $faker->numberBetween(100, 1000),
        'discount' => $faker->numberBetween(1, 100)
    ];
});
