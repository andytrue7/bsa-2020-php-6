<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $buyers = factory(\App\Buyer::class, 5)->create();

        $buyers->each(function ($buyer){

            $orders = factory(\App\Order::class, 5)->create(['buyer_id'=>$buyer->id]);

            $orders->each(function ($order){
                $items = factory(\App\OrderItem::class, 3)->create(['order_id'=>$order->id]);
                $order->orderItems()->saveMany($items);
            });

            $buyer->orders()->saveMany($orders);

        });
    }


}
